---
title: "Example sub-model explanation"
output:
  html_document:
    df_print: paged
  html_notebook:
    default
---
# Intro

In order to facilitate the writing, debugging and analysis of the submodels is important to have a good structure. This is also crucial to be able to **merge** the different submodels in one model.

## Functions
The basic bulding block for have a good structure of the model is using *functions*, functions allow to have a block of code that take some inputs, perform calculations and return an output. This is very important to divide the code in sections that are easy to understand and reuse.
Moreover, by using a common structure of funtions the different sub-models will be consistent.
You can find a good all what you need to know about functions in R at [https://r4ds.had.co.nz/functions.html](https://r4ds.had.co.nz/functions.html)

## Equations

A lot of the code will be implementing equations into code, there is would be very important that you write the number of the equation that you are using in a comment.
In general try to comment code to explain why and what you are doing

```
# This is a comment from equation 99.100
```

# Model setup

To have consistency in the model it is important to have a common way to handle parameters and actually running the model code.

Therefore at the beginning of your file add those lines of code:

```{r, warning=FALSE, message=FALSE}
# This defines the `pars` variable to access parameters
source(here::here("utility/setup_parameters.R"))
# This defines the `run_model` function to run a (sub)model using the default input and state
source(here::here("utility/model_runner.R"))
```


# Defining sub-model functions

The first step is to actually write the functions of the sub-models components.
This functions should be small (~10 lines of code) and do exactly one thing.
In doubt write another function ;)

The function should explicitely take as an argument all the variable from the input/state that they need for computation. The should return the individual variable calculated, is case of multiple values use a list.

Parameters **should not** be give as a function argument, but directly accessed in the function body using `pars`. If you need some other parameter add it to the `Measurements/parameters.csv` file and it will automatically included into `pars`.

This is an example of such a function. Note your sub-model will have several of this small functions.

:::rmdinfo
To avoid creating conflicts with the actual model the parameters and the state used in the model have the prefix `ex`
:::

```{r}
#' This returns the leaf carbon at the next time step, given the current leaf carbon
#' Example function take from the carbon cycle model
leaf_carbon_update <- function(leaf_carbon){ 
  leaf_carbon_next <- (leaf_carbon + leaf_carbon * pars$ex_input_l) - leaf_carbon * pars$ex_output_l 
  return(leaf_carbon_next)
}
```

# Sub-model `step` function

Each sub-model should have at least one `..._step` (eg. `radiative_transfer_step` or `carbon_cycle_step`) function that is the main entry point of the sub-model and takes care of running of the sub-models components in the right order and calculate the finale state.

This function **must have** have standard way of dealing with input and output.

The functions takes has two arguments:

- `input`. This is a dataframe that contains only 1 row, where the columns are the various input variables. The input is by definitions variable that change at every time step (unlike parameters) and their values are needed to run the model, they derive from a measure in the field or by previously run sub-models. A good example is the incoming radiation, that is a necessary input of the radiative transfer model, or the precipitation.
- `state`. This is a dataframe that contains only 1 row with all the state variables. State variables are the output of the model at the previous time step that is then then used an input. The state is a property of the ecosystem that evolves over time.
Good examples of a state variable is the temperature or the amount of carbon in a leaf.
State needs to initialized to a starting value, you can do it in the file `Measurements/initial_state.csv` (this is used by `model_runner.R`)

The function must return a list with two elements:

- `output`. This is a one row dataframe that contains all the variables calculated from the sub-model. Note the output is not relevant at the next time step, but can be used an an input by other sub-models, for example the amount of radiation abosrbed by the canopy is an output of radiative transfer but an input for photosynthesis.
- `state`. This is a one row dataframe that contains the updated state at the current time step. Note it should not contain contains column that were not already present in the state dataframe.

*Important*. This function should model what happens at one time step, so you should not use have a loop over time nor directly read the input data.

This is an example of such a step function:
```{r}
leaf_carbon_step <- function(input, state){
  output <- tibble(TS = input$TS_2cm_degC) # in this just a silly way of calculating the output
  state$ex_leaf_carbon <- leaf_carbon_update(state$ex_leaf_carbon)
  # this needs to be a list with the first element named `output` and the second `state`
  return(list(
    output = output,
    state = state
  ))
}
```

# Model runner

After building the sub-models you can run it using the `run_model` functions.
It takes care of reading the input data, initial state and properly run the sub-model step and collecting the output. This is the suggested way of running your sub-model

`run_model` takes the following arguments:

- `model_step`. This is the "step" function name that is going to actually run the code

optional arguments:

- `input`. The default is the gapfilled input, but if you have some requirement for your input data you can provide your own dataframe
- `state`. The default is from `utility/initial_state.csv` but you can also provide your own dataframe.

*Note* when you are testing your model you don't want to run it with the whole dataset because it takes time. You can use `run_model` with the `input_test` as input, that contains the first 1000 rows of the input.


The function `run_model` returns a list of:
 
 - `output`. A dataframe with the all the outputs at different time steps
 - `state`. A dataframe with the all the outputs at different time steps

:::rmdwarning
This function use a bit advanced concept in R, you can give a function as an argument to `run_model`, but you don't need to worry about it. Just use the function name without the parenthesis.
:::

This is an example of how you can use the `run_model` function

```{r}
with_progress(leaf_model_output <- run_model(leaf_carbon_step), handler_rstudio()) # Note there are no parenthesis () after the `leaf_carbon_step` function
```

# Output

the output from `run_model` can then be analysed.

For example here we are plotting the leaf carbon over time

```{r}
state <- leaf_model_output$state

ggplot(state) +
  geom_line(aes(TIMESTAMP_END, ex_leaf_carbon))
```







