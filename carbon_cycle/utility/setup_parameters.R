# to use this file put at the top of your script
# source(here::here("utility/setup_parameters.R"))
library(readr)
library(tidyr)
library(dplyr)
library(magrittr)

# Parameter ------------

# pars_all is a dataframe with all the parameters info. Should not be directly used 
pars <- read_csv(here::here("Measurements/parameters.csv"), show_col_types = FALSE) %>% 
  select(variable, value) %>% 
  pivot_wider(names_from = variable, values_from = value)

# the `pars` object is accessible from everyone
# you can access the individual parameters using the $ syntax
# for example pars$o2_atm




