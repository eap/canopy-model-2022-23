# setup -------------------------------------------------------------------
library(Metrics)
library(nloptr)


load("soil_models.RData")

save.image("soil_models.RData")

fluxes <- read.table(file = "Measurements/Measurements_fluxes_hourly_201601_201712_gapfilled.csv", header = T, sep = ",")
meteo <- read.table("Measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv", header = T, sep = ",")
soil <- read.table("Measurements/Measurements_soil_hourly_201601_201712_gapfilled.csv", header = T, sep = ",")

{
  soil$TIMESTAMP_END = as.POSIXct(soil$TIMESTAMP_END, format = "%Y-%m-%d %H:%M")
  meteo$TIMESTAMP_END = as.POSIXct(meteo$TIMESTAMP_END, format =  "%Y-%m-%d %H:%M")
  fluxes$TIMESTAMP_END = as.POSIXct(fluxes$TIMESTAMP_END, format =  "%Y-%m-%d %H:%M")
}

{
  range <- 1:17000
  par(mar=c(4,4,1,1))
  plot(meteo$TIMESTAMP_END[range], meteo$PREC_mm[range]*5, type = "l", col = "blue", ylim = c(0,50))
  lines(soil$TIMESTAMP_END[range], soil$SWC_8cm_.[range], col = "cyan", cex=0.5)
  lines(soil$TIMESTAMP_END[range], soil$SWC_16cm_.[range], col = "red", cex=0.5)
  lines(soil$TIMESTAMP_END[range], soil$SWC_32cm_.[range], col = "purple", cex=0.5)
  axis(4)
}

## parameters
z1_m <- 0.12 # 12cm for first layer (middle between 8 & 16cm)
S_max <-  0.50*z1_m # Saturation
FC <-  0.35*z1_m  # Field capacity
PWP <-  0.20*z1_m # Permanent Wilting Point
D_max <-  25 # Maximum daily drainage rate

pore_vol_m <- 0.50*z1_m
pore_vol_mm <- 1000*pore_vol_m
water_vol_mm <- pore_vol_mm*0.5 # 50% sat

SWC <- water_vol_mm/pore_vol_mm 



# PET hourly --------------------------------------------------------------
zsl <- 450
delta <- 4098*(0.6108*exp((17.27*Tair)/(T+237.3)))/(Tair + 237.3)^2
P <-  101.3*((293-0.0065*zsl)/293)^5.26 # air pressure [KPa] at z m above sea level
gamma <-  0.665*10^(-3)*P # saturation slope vapour pressure curve

Tair <- meteo$TA_degC # air temperature
RH <- meteo$RH_. # relative humidity
es <-  0.6108 * exp(17.27*Tair/(Tair+237.3)) # sat vapour pressure
ea <- RH/100*(es) # act vapour pressure
Rn_Wm2 <- (meteo$SW_IN_Wm.2 - meteo$SW_OUT_Wm.2) + (meteo$LW_IN_Wm.2 - meteo$LW_OUT_Wm.2)
Rn_MJm2 <- Rn_Wm2*3600/1000000
L_Wm2 <- fluxes$LE_Wm.2
L_MJm2 <- L_Wm2*3600/1000000 # latent heat flux
H_Wm2 <- fluxes$H_Wm.2
H_MJm2 <- H_Wm2*3600/1000000 # sensible heat flux
## Rn = L + H + G net rad = sum of latent, sensible and ground heat flux
## G  = RN - L - H
G_MJm2 <- Rn_MJm2 - L_MJm2 - H_MJm2

Wspd <- meteo$WS_ms.1
u2 <- Wspd * 4.87/log(67.8*10 -5.42) # Wind speed [m/s] at 2m above ground

PET = (0.408*delta*(Rn_MJm2-0)+gamma*(37/(T+273))*u2*(es-ea))/
  (delta+gamma*(1+0.34*u2))



plot(meteo$TIMESTAMP_END, PET, type = "l")
lines(meteo$TIMESTAMP_END, meteo$PREC_mm, col = "blue")
plot(Wspd, u2)
sum(meteo$PREC_mm)
sum(PET)

# 1st layer 0-12cm----------------------------------------------------
soil_vol_mm <- 120
SAT <- 0.50 # saturation = 100% pore volume
PWP <- 0.18 # permanent wilting point
FC <- 0.4101467 # opt: 0.4101467 # 0.4

## scale infiltration between 0.1 at min LAI and 0.4 at max LAI
LAI <- get_day_LAI(soil$TIMESTAMP_END)
interception_perc <- LAI * LAI_scale_opt[1] + LAI_scale_opt[2]
plot(interception_perc, type = "l")
prec_int <- meteo$PREC_mm * (1-interception_perc)

prec_int <- meteo$PREC_mm * (1 - LAI * 0.01 + 0.3)
plot(meteo$PREC_mm * (1-LAI* 0.01 + 0.3))

## Drainage rate above FC
Drainage_mm <- 0.00106209# 0.001
  
## top layer due to PET being subtracted 
## no less than PWP due to transpiration
SAT <- 0.5 # saturation = 100% pore volume
PWP <- 0.2 # permanent wilting point


f.SWC_8 <- function(SWC_start, prec, PET, LAI_scale){
  SWC_pred <- SWC_start
  Runoff_pred <- rep(0, length(prec)+1)
  Drainage_pred <- rep(0, length(prec)+1)
  prec_int <- prec * (1 - (LAI * LAI_scale[1] + LAI_scale[2]))
  for (i in 1:length(prec)){
    SWC_pred[i+1] = SWC_pred[i] + (prec_int[i] - PET[i]*0.8)/soil_vol_mm 
    if (SWC_pred[i+1] > SAT){ # no more than 100% sat
      Runoff_pred[i+1] = (SWC_pred[i+1] - SAT)*soil_vol_mm
      SWC_pred[i+1] = SAT
    } else if(SWC_pred[i+1] > FC){
      Drainage_pred[i+1] = Drainage_mm
      SWC_pred[i+1] = SWC_pred[i+1] - Drainage_mm
    } else if (SWC_pred[i+1] < PWP){ # no less than 0% or PWP due to transpiration
      SWC_pred[i+1] = PWP
    }
  }
  cbind.data.frame(SWC_pred, Drainage_pred)
}


## optimization
obj_crit <- function(x){
  param <- x
  SWC_pred <-  f.SWC(0.39648, meteo$PREC_mm, PET, param)[,1]
  RMSE <- rmse(soil$SWC_8cm_., SWC_pred[-1]*100)
  return(RMSE)
}
LAI_scale <- c(0.01, 0.3)
mod_calib <- neldermead(LAI_scale, obj_crit, control = list(maxeval = 1000))
mod_calib$par

LAI_scale_opt <- mod_calib$par # 0.04871071 -0.02729716
# soil$SWC_8cm_.[1]/100 # SWC_start: 0.39648
# df_SWC_PET <- f.SWC(0.39648, meteo$PREC_mm, PET)

df_SWC_ET8 <- f.SWC(0.39648, meteo$PREC_mm, ET, LAI_scale)

{
range <- 1:17544
plot(meteo$TIMESTAMP_END[range], soil$SWC_8cm_. [range], type = "l", ylim = c(0,SAT*100))
lines(meteo$TIMESTAMP_END[range], df_SWC_ET8$SWC_pred[range]*100, col = "red")
# lines(meteo$TIMESTAMP_END[range], soil$TS_2cm_degC[range], col = "blue")
}

sum(df_SWC_ET8$Drainage_pred)

# 2nd layer 12-24cm----------------------------------------------------------
soil_vol_mm <- 120
SAT <- 0.50 # saturation = 100% pore volume
PWP <- 0.18 # permanent wilting point
FC_16 <- 0.4101467 # opt: 0.4101467 # 0.4

f.SWC_16 <- function(SWC_start, prec, PET, LAI_scale){
  SWC_pred <- SWC_start
  Runoff_pred <- rep(0, length(prec)+1)
  Drainage_pred <- rep(0, length(prec)+1)
  prec_int <- prec*0.8 * (1 - (LAI * LAI_scale[1] + LAI_scale[2]))
  for (i in 1:length(prec)){
    SWC_pred[i+1] = SWC_pred[i] + (prec_int[i] - PET[i]*0.7)/soil_vol_mm 
    if (SWC_pred[i+1] > SAT){ # no more than 100% sat
      Runoff_pred[i+1] = (SWC_pred[i+1] - SAT)*soil_vol_mm
      SWC_pred[i+1] = SAT
    } else if(SWC_pred[i+1] > FC_16){
      Drainage_pred[i+1] = Drainage_mm
      SWC_pred[i+1] = SWC_pred[i+1] - Drainage_mm
    } else if (SWC_pred[i+1] < PWP){ # no less than 0% or PWP due to transpiration
      SWC_pred[i+1] = PWP
    }
  }
  cbind.data.frame(SWC_pred, Drainage_pred)
}

df_SWC_ET16 <- f.SWC_16(0.38290, meteo$PREC_mm, ET, LAI_scale)


{
  range <- 1:17544
  plot(meteo$TIMESTAMP_END[range], soil$SWC_16cm_. [range], type = "l", ylim = c(0,SAT*100))
  lines(meteo$TIMESTAMP_END[range], df_SWC_ET16$SWC_pred[range]*100, col = "red")
  lines(meteo$TIMESTAMP_END[range], soil$TS_2cm_degC[range], col = "blue")
  # lines(meteo$TIMESTAMP_END[range], meteo$PREC_mm[range]*2)
  lines(meteo$TIMESTAMP_END[range], ET[range]*10)
  
  lines(meteo$TIMESTAMP_END[range], interception_perc[range]*20)
}

plot(ET, type = "l")

prec <- meteo$PREC_mm

prec1 <- prec/3
prec2 <- c(0,prec/3)
prec2 <- prec2[1:17544]
prec3 <- c(0,0,prec/3)
prec3 <- prec3[1:17544]

prec_inf_3h <- prec1 + prec2 + prec3

sum(prec)
sum(prec_inf_3h)

# Validation --------------------------------------------------------------

## act vs pred
par(pty = "s")
plot(soil$SWC_8cm_.[range], df_SWC_ET$SWC_pred[range]*100,
     ylim = c(15,50), xlim = c(15,50), pch = 19, cex = 0.5,
     xlab = "actual SWC [%]",
     ylab = "predicted SWC [%]")
dev.off()

rmse(soil$SWC_8cm_., df_SWC_ET$SWC_pred[-1]*100)





## estimate ET from latent heat and lambda
## latent heat W/m2 to J/h/m2
L_Jm2h <- fluxes$LE_Wm.2 * 3600

ET <- L_Jm2h / lambda_h

plot(meteo$TIMESTAMP_END[range], ET[range], type = "l")
lines(meteo$TIMESTAMP_END[range], PET[range], col = "red")

lambda_h <- get_lambda(temp = meteo$TA_degC)

get_lambda <- function(temp=NULL){
  if (is.null(temp)){
    lambda = 2.45*10**6
  }
  else{
    lambda = 2.501*10**6 - 2361.*temp  
  }
  return(lambda)
}


plot(LAI)
min_int <- 0.1
max_int <- 0.4

soil$SWC_8cm_.[1]/100 # SWC_start: 0.39648
df_SWC <- f.SWC(0.39648, meteo$PREC_mm, PET)

{
range <- 1:17545
plot(meteo$TIMESTAMP_END[range], df_SWC$SWC_pred[range]*100, type = "l")
lines(meteo$TIMESTAMP_END[range], soil$SWC_8cm_. [range], col = "blue")
}

## top layer due to PET being subtracted 
f.SWC <- function(SWC_start, prec, PET){
  SWC_pred <- SWC_start
  Drainage_pred <- rep(0, length(prec)+1)
  for (i in 1:length(prec)){
    SWC_pred[i+1] = SWC_pred[i] + (prec[i] - PET[i])/soil_vol_mm 
    if (SWC_pred[i+1] > SAT){ # no more than 100% sat
      Drainage_pred[i+1] = (SWC_pred[i+1] - SAT)*soil_vol_mm
      SWC_pred[i+1] = SAT
    } else if (SWC_pred[i+1] < PWP){ # no less than 0% or PWP
      SWC_pred[i+1] = PWP
    }
  }
  cbind.data.frame(SWC_pred, Drainage_pred)
}


# dump --------------------------------------------------------------------


df_prec <- meteo$PREC_mm 

df_prec <- meteo$PREC_mm
FC
f.SWC_prec <- function(SWC_start, prec, pore_vol_mm){
  SWC_pred <- c(SWC_start)
  Drainage_pred <- rep(0, length(prec)+1)
  for (i in 1:length(prec)){
    SWC_pred[i+1] = SWC_pred[i] + prec[i]/pore_vol_mm
    if (SWC_pred[i+1]>){
      Drainage_pred[i+1] = (SWC_pred[i+1] - 1)*pore_vol_mm
      SWC_pred[i+1] = 1
    }
  }
  cbind.data.frame(SWC_pred, Drainage_pred)
}


df_SWC_pred <- f.SWC_prec(SWC, df_prec, 60)  

plot(1:1000, df_SWC_pred$SWC_pred[1:1000]) #SWC hits 1, rest will be drainage


