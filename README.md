# Canopy Model 2021-22

This is the repository for the canopy model developed as part of the Ecosystem Atmosphere Processes course for winter semester 2022/23


The folder `utility` contains some helper functions to build sub-models

In particular check `utility/example_model.Rmd` for a guide on how to create a sub-model.