library(readr)
library(tidyr)
library(dplyr)
library(magrittr)
library(scales)
library(zoo)

source("leaf_temperature/leafTemperature.R")
source("leaf_temperature/newtonMethod.R")

fluxes <- read_csv(here::here("Measurements/Measurements_fluxes_hourly_201601_201712_gapfilled.csv"), show_col_types = FALSE)
meteo <- read_csv(here::here("Measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv"), show_col_types = FALSE)

meteo %>% head()
fluxes %>% head()

leafTempParameters <- merge(fluxes, meteo, by="TIMESTAMP_END") %>% 
  select(c( "TA_degC", "TIMESTAMP_END", "SW_IN_Wm-2", "SW_OUT_Wm-2", "LW_IN_Wm-2", "LW_OUT_Wm-2", "LE_Wm-2", "H_Wm-2", "PRESS_hPa" ,"RH_%", "WS_ms-1"))

ltemp_init <- C2K(21.0)
ltemp_last <- ltemp_init

run  <- function(leafTempParameters) { # iter with last 
  ltemp = c()
  for (i in 1:nrow(leafTempParameters)){
    #(sw_in, sw_out, lw_in)
    
    row <- leafTempParameters[i,]
    #print(row)
    
    T_a <- row$`TA_degC`
    sw_in <- row$`SW_IN_Wm-2`
    sw_out <- row$`SW_OUT_Wm-2`
    lw_in <- row$`LW_IN_Wm-2`
    pressure <- row$`PRESS_hPa`
    rh <- row$`RH_%`
    wind_speed <- row$`WS_ms-1`
    
    #H <- row$`H_Wm-2`
    #lE <- row$`LE_Wm-2`
    
    f <- leafTemperatur(C2K(T_a), sw_in, sw_out, lw_in, pressure, rh, wind_speed)
    df <- derivative(f, 0.0001)
    
    #print("ltemp_last")
    #print(ltemp_last)
    res <- newton(f, df, ltemp_last)
    ltemp <- c(ltemp, res["x"])
    
    #print(res)
    ltemp_last <- res[["x"]]
    cat(i / nrow(leafTempParameters))
    cat("\r")
  }
  
  leafTempParameters$temperature_K <- ltemp
  leafTempParameters
}
ltv <- run(leafTempParameters)
ltv$temperature_C <- ltv$temperature_K - 273.15
ltv$at_K <- ltv$TA_degC + 273.15

ltv %>% print()


plot(ltv$temperature_C, col=alpha("black",0.1), pch=19)
points(ltv$TA_degC, col=alpha("red",0.1), pch=19)

lt <- ltv %>% 
  group_by(hour=hour(TIMESTAMP_END)) %>% 
  summarize(mean_lt=mean(temperature_K), sd_lt=sd(temperature_K))

print(lt)

plot(lt$hour, lt$mean_lt, col=alpha("black", 0.9), pch=19)
plot(lt$hour, lt$sd_lt, col=alpha("red", 0.9), pch=19)

at <- ltv %>%
  group_by(hour=hour(TIMESTAMP_END)) %>% 
  summarize(mean=mean(at_K), sd=sd(at_K))

plot(lt$hour, lt$mean_lt, col=alpha("green", 0.9), pch=19)
points(at$hour, at$mean, col=alpha("blue", 0.9), pch=19)


ltv$h <- hour(ltv$TIMESTAMP_END)

v <- ltv[ltv$h == 12,]
plot(v$temperature_C, col=alpha("black",0.2), pch=19)
points(v$TA_degC, col=alpha("red",0.2), pch=19)

v <- ltv[ltv$h == 0,]
plot(v$temperature_C, col=alpha("black",0.2), pch=19)
points(v$TA_degC, col=alpha("red",0.2), pch=19)


v <- ltv[ltv$h == 12,]
k = 28
v$lt <- rollmean(v$temperature_C, k=k, fill=mean(v$temperature_C))
v$at <- rollmean(v$TA_degC, k=k, fill=mean(v$TA_degC))

plot(v$TIMESTAMP_END, v$at,
     col= ifelse(at >= 20, "red", ifelse(at >= 15 ,"orange",ifelse(at >= 10, "yellow"
        , ifelse(at >= 5, "green", ifelse(at >= 0, "lightblue", ifelse(at >= -5, "blue", ifelse(at >= -5, "darkblue", ifelse(at >= -15, "black")))))))))
          


plot(v$lt, col= ifelse(at >= 20, "red", ifelse(at >= 15 ,"orange",ifelse(at >= 10, "yellow"
                                                                           , ifelse(at >= 5, "green", ifelse(at >= 0, "lightblue", ifelse(at >= -5, "blue", ifelse(at >= -5, "darkblue", ifelse(at >= -15, "black")))))))))

