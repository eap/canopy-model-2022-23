library(tidyverse)
library(here)
library(progressr)

# initial_all is a dataframe with all the parameters info. Should not be directly used 
initial_state <- read_csv(here::here("Measurements/initial_state.csv"), show_col_types = FALSE) %>% 
  select(state_variable, value) %>% 
  pivot_wider(names_from=state_variable, values_from = value)

  
input_flux <- c("Measurements/Measurements_fluxes_hourly_201601_201712_gapfilled.csv") %>% 
  here() %>% 
  read_csv(show_col_types = FALSE)

input_meteo <- c("Measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv") %>% 
  here() %>% 
  read_csv(show_col_types = FALSE)

input_soil <- c("Measurements/Measurements_soil_hourly_201601_201712_gapfilled.csv") %>% 
  here() %>% 
  read_csv(show_col_types = FALSE)

input_gap <- input_flux %>% 
  inner_join(input_meteo, by = "TIMESTAMP_END") %>%
  inner_join(input_soil, by = "TIMESTAMP_END")
  
input_test <- head(input_gap, 1000)

run_model <- function(model_step, input=input_gap, state=initial_state){
  
  #setup progress 
  p <- progressor(nrow(input))
  # Run the first time to initialize output and deal with previous state
  step_out <- model_step(slice_head(input), initial_state)
  output <- step_out$output %>%
    as_tibble() 
  state <- step_out$state %>%
    as_tibble()
  
  
  # Form the second time step run the model using the loop
  
  for(i in 2:nrow(input)){
    # call the model step with one row of input data. Note the state is from the previous time step
    step_out <- model_step(slice(input,i), slice(state, i-1))
    
    output[i,] <- step_out$output %>% 
      as_tibble() 
    state[i,] <- step_out$state %>% 
      as_tibble()
    
    p()
  }
  
  # Add the timestamp to output and state 
  timestamp <- select(input, TIMESTAMP_END)
  output <- bind_cols(timestamp, output)
  state <- bind_cols(timestamp, state)
  
  return(list(output = output, state = state))
}