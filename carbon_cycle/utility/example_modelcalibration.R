rm(list = ls())

# Simple soil organic carbon decomposition
# in a lab situation

# meas of SOC
mon <- c(1,13,25,37,49,61,73,85,97,109,121)
SOC_obs <- c(12.18685006,8.74837702,2.20475992,0.40799748,3.02876043,2.34040187,
             2.25946497,2.99085747,0.30671536,1.48960887,0.03675738)
obs <- as.data.frame(cbind(mon, SOC_obs))


# Model Function
SOC_model <- function(time_i, SOC_i, k_i){
  # initial values
  SOC <- SOC_i          # soil organic carbon (g C/(kg soil))
  k <- k_i              # turnover rate (-)
  time <- time_i        # time
  
  # SOC decomposition for each time step
  for(i in time){
    SOC[i+1] <- SOC[i] - k * SOC[i]
  }
  
  # make output nice
  time[121] <- 121
  time <- time-1
  results <- as.data.frame(cbind(time, SOC))
  return(results)
}


# initial values
SOC <- 10
k <- 0.1
time <- seq(1,120)   # 120 months = 10 years

# model run
mod_output <- SOC_model(time, SOC, k)

# plot
plot(mod_output$time, mod_output$SOC, col='red', ylim=c(0,13))
points(obs$mon, obs$SOC_obs, col='black')


#--------------------------------------------------------------
# Model Calibration
# The parameter k was estimated based on literature, which could be faulty.
# To increase the model fit/improve the match between model and measurements,
# we adjust the parameter k.
# The objective criterion is RMSE.

library(nloptr)

obj_crit <- function(x){
  # meas of SOC
  mon <- c(1,13,25,37,49,61,73,85,97,109,121)
  SOC_obs <- c(12.18685006,8.74837702,2.20475992,0.40799748,3.02876043,2.34040187,
               2.25946497,2.99085747,0.30671536,1.48960887,0.03675738)
  obs <- as.data.frame(cbind(mon, SOC_obs))
  
  # initial values
  SOC_i <- 10
  k_i <- x
  time_i <- seq(1,120)
  
  # model run
  mod_output <- SOC_model(time_i, SOC_i, k_i)
  
  # Comparison with observations
  sim <- mod_output[obs$mon,]
  rmse = sqrt(mean((obs$SOC_obs - sim$SOC)**2))   # g C/(kg soil)
  return(rmse)
}

# run Nelder-Mead Simplex to minimize RMSE
mod_calib <- neldermead(k, obj_crit)


# plot results of model calibration
# initial values
SOC <- 10
k <- mod_calib$par
time <- seq(1,120)

# model run
mod_output <- SOC_model(time, SOC, k)

# plot
plot(mod_output$time, mod_output$SOC, col='red', ylim=c(0,13))
points(obs$mon, obs$SOC_obs, col='black')

sim <- mod_output[obs$mon,]
plot(obs$SOC_obs, sim$SOC)